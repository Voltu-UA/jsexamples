After you clone the repository you should see 3 projects:

1. **ColorGame** - which allows you to guess an RGB color scheme.

2. **Tap** - which allows you to type any key(with alphabetic symbols) from the keyboard and to see the bubble effect with the corresponding sound.

3. **ToDoList** - implementation of the to-do list.

To run any of the above listed projects, just run the corresponding **.html** file from the root directory of the specific project.